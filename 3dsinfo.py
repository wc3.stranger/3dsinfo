#!/usr/bin/env python3

__appname__ = '3-D Secure info'
__version__ = '0.1'


import os
import sys
import re
import base64
import zlib
import datetime
import functools
from decimal import Decimal
from collections import OrderedDict as odict
import xml.dom.minidom
import xml.etree.ElementTree as ET

import pytz
from OpenSSL import crypto
from pygments import highlight
from pygments.lexers import XmlLexer
from pygments.formatters import HtmlFormatter

from PyQt5.QtCore import Qt, QTranslator, QLocale, pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import (
    QWidget, QTextEdit, QGridLayout, QApplication, QPushButton, QMessageBox, QMainWindow,
    QSplitter, QVBoxLayout, QHBoxLayout, QMenu, QAction, QSystemTrayIcon,
)
from PyQt5.QtGui import QTextDocument, QIcon, QTextCursor, QKeyEvent, QClipboard


pre_style_fmt = '''
    pre {{
        display: block;
        font-family: "{0}";
        white-space: pre;
        margin: 1em 0;
    }}
    .cp {{ color: #BC7A00 }} /* Comment.Preproc */
    .na {{ color: #7D9029 }} /* Name.Attribute */
    .nt {{ color: #008000; font-weight: bold }} /* Name.Tag */
    .m {{ color: #666666 }} /* Literal.Number */
    .s {{ color: #BA2121 }} /* Literal.String */
'''

qt_app = None
langs_dir = None
locale_if_no_translation = 'en_US'

currencies = {
    '643': 'RUB', '840': 'USD', '978': 'EUR', '860': 'UZS', '398': 'KZT', '036': 'AUD',
}

translators = []


def addTranslation(func, trans_func=None):
    translators.append((func, trans_func,))
    # @note: translate now
    if callable(trans_func):
        return func(trans_func())
    return func()


def get_cert_info(content, translate=False):
    content_formatted = '-----BEGIN CERTIFICATE-----\n{}\n-----END CERTIFICATE-----\n'.format(
        content.strip(),
    )
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, content_formatted)

    info = {
        'signature_algo': cert.get_signature_algorithm().decode(),
    }

    not_before = re.sub('^(\d+)Z$', '\\1GMT', cert.get_notBefore().decode())
    tz = pytz.timezone(re.search('^\d+([^d]+)$', not_before).group(1))
    # @note: Z (Zulu) timezone is unknown for datetime format (it is UTC actually)
    info['not_before'] = datetime.datetime.strptime(not_before, '%Y%m%d%H%M%S%Z').replace(tzinfo=tz)

    not_after = re.sub('^(\d+)Z$', '\\1GMT', cert.get_notAfter().decode())
    tz = pytz.timezone(re.search('^\d+([^d]+)$', not_after).group(1))
    # @note: Z (Zulu) timezone is unknown for datetime format (it is UTC actually)
    info['not_after'] = datetime.datetime.strptime(not_after, '%Y%m%d%H%M%S%Z').replace(tzinfo=tz)

    pub_key = cert.get_pubkey()
    info['pub_key_bits'] = pub_key.bits()
    if pub_key.type() == crypto.TYPE_RSA:
        info['pub_key_type'] = 'rsaEncryption'
    else:
        info['pub_key_type'] = 'dsaEncryption'

    trans_table = (
        ('C', '(C) Country Name'),
        ('ST', '(ST) State or Province Name'),
        ('L', '(L) Locality Name (eg, city)'),
        ('O', '(O) Organization Name (eg, company)'),
        ('OU', '(OU) Organizational Unit Name (eg, section)'),
        ('CN', '(CN) Common Name'),
        ('emailAddress', 'Email Address'),
    )

    info['issuer'] = odict()
    issuer = cert.get_issuer()
    info['subject'] = odict()
    subject = cert.get_subject()

    for field, t_field in trans_table:
        # @warn: field/value may have non-utf8 encodings
        info['issuer'][t_field if translate else field] = getattr(issuer, field, '') or ''
        info['subject'][t_field if translate else field] = getattr(subject, field, '') or ''

    return info


def pre_format_output(msg):
    return '<pre style="font-family: {}">{}</pre>'.format(
        qt_app.font().family(), msg.strip(),
    )


def amount_value(value, exp_node, currency_node):
    amount = Decimal(value)
    if exp_node is not None:
        amount = '{:.2f}'.format(amount / (10**Decimal(exp_node.text)))

    cur_code = currency_node.text
    if cur_code not in currencies:
        value = '<b>{} / {}</b>'.format(amount, cur_code)
    else:
        value = '<b>{} {}</b>'.format(amount, currencies[cur_code])

    return value


def pan_value(value):
    if value.replace('0', ''):
        return '<b>******{}</b>'.format(value.lstrip('0'))
    return '&ndash;'


def dt_value(value):
    dt = datetime.datetime.strptime(value, '%Y%m%d %H:%M:%S')
    return '<b>{}</b>'.format(dt.strftime('%d %b %Y %H:%M:%S'))


def build_msg(node, info_nodes):
    msg = ''
    for node_path, node_title, value_format, *extra_nodes in info_nodes:
        node_found = node.find(node_path)
        if node_found is not None:
            if callable(value_format):
                extra_values = (node.find(p) for p in extra_nodes)
                msg = '{}{}: {}\n'.format(
                    msg, node_title, value_format(node_found.text, *extra_values),
                )
            elif value_format is not None:
                msg = '{}{}: {}\n'.format(
                    msg, node_title, value_format.format(node_found.text),
                )
            else:
                msg = '{}{}: {}\n'.format(msg, node_title, node_found.text)

    return msg


def decode_pareq(msg_node):
    node = msg_node.find('PAReq')

    def exp_date_value(value):
        return '<b>{}/20{}</b>'.format(value[-2:], value[:2])

    # @note: <node_path>, '<node_title>', '<value_format>'
    info_nodes = (
        (
            './Purchase/purchAmount', '%% Amount %%', amount_value,
            './Purchase/exponent', './Purchase/currency',
        ),
        ('./Purchase/date', '%% Purchase date %%', dt_value),
        ('./CH/acctID', '%% Client account %%', '<b>{}</b>'),
        ('./CH/expiry', '%% Card expiry date %%', exp_date_value),
        ('./Purchase/xid', 'XID', '<b>{}</b>'),
        ('./Merchant/acqBIN', '%% Acquirer BIN %%', '<b>{}</b>'),
        ('./Merchant/name', '%% Merchant name %%', '<b>{}</b>'),
        ('./Merchant/merID', '%% Merchant ID %%', '<b>{}</b>'),
        ('./Merchant/url', '%% Merchant URL %%', '<b>{}</b>'),
    )

    msg = build_msg(node, info_nodes)

    return pre_format_output(msg)


def decode_pares(msg_node):
    node = msg_node.find('PARes')

    # @note: <node_path>, '<node_title>', '<value_format>'
    info_nodes = (
        ('./Error/errorCode', '%% Error code %%', '<font color="red"><b>{}</b></font>'),
        ('./Error/errorMessage', '%% Error message %%', '<b>{}</b>'),
        ('./Error/errorDetail', '%% Details %%', '<b>{}</b>'),
        ('./TX/status', '%% 3DS authentication status %%', '<b>{}</b>'),
        ('./IReq/iReqCode', '%% Error code %% (IReq)', '<b>{}</b>'),
        ('./IReq/iReqDetail', '%% Error details %% (IReq)', '<b>{}</b>'),
        ('./Merchant/acqBIN', '%% Acquirer BIN %%', '<b>{}</b>'),
        ('./Merchant/merID', '%% Merchant ID %%', '<b>{}</b>'),
        ('./TX/eci', 'ECI', '<b>{}</b>'),
        ('./Purchase/xid', 'XID', '<b>{}</b>'),
        ('./TX/cavv', 'CAVV', '<b>{}</b>'),
        ('./Purchase/date', '%% Purchase date %%', dt_value),
        ('./TX/time', '%% Transaction date %%', dt_value),
        (
            './Purchase/purchAmount', '%% Amount %%', amount_value,
            './Purchase/exponent', './Purchase/currency',
        ),
        ('./pan', '%% PAN mask %%', pan_value),
    )

    msg = build_msg(node, info_nodes)

    # @warn: may be use lxml is better choice?..
    certs = re.findall(
        '<[^\/]*?X509Certificate>(.+?)</[^>]*?X509Certificate>', ET.tostring(msg_node).decode()
    )
    subjects = []
    issuers = []
    certs_msg = ''
    if not len(certs):
        msg = '{}<font color="blue"><b>%% Certificates %%</b></font>: '\
              '<font color="red"><b>%% not found %%</b></font>\n'.format(msg)
    else:
        for i, f_content in enumerate(certs):
            try:
                info = get_cert_info(f_content)
                issuers.append(info['issuer'])
                subjects.append(info['subject'])

                certs_msg = '{}<font color="blue"><b>%% Certificate %% {}</b></font> (subject):\n'\
                      '  %% organization %% &ndash; <b>{} ({})</b>\n'\
                      '  %% organization unit %% &ndash; {}\n'\
                      '  %% common name %% &ndash; {}\n'.format(
                    certs_msg, i + 1, info['subject']['O'], info['subject']['C'],
                    info['subject']['OU'], info['subject']['CN'],
                )
            except Exception as e:
                certs_msg = '{}%% Certificate %% {}: <font color="red"><b>{}</b></font>\n'.format(
                    certs_msg, i + 1, e,
                )

    # @note: detect last and first certificate in chain
    card_issuer = card_system = None
    for i in range(len(issuers)):
        if subjects[i] == issuers[i]:
            # @note: self-signed certificate, means "card payment system"
            card_system = issuers[i]['O']
        elif subjects[i] not in issuers:
            card_issuer = subjects[i]['O']

    if card_system:
        msg = '{}%% Payment system %%: <b>{}</b>\n'.format(msg, card_system)
    if card_issuer and card_issuer != card_system:
        msg = '{}%% Issuer (possible) %%: <b>{}</b>\n'.format(msg, card_issuer)

    msg = '{}\n{}'.format(msg, certs_msg.strip())

    return pre_format_output(msg)


def decode_input(xml_str):
    root = ET.fromstring(xml_str)

    assert root.tag.lower() == 'threedsecure'
    assert len(root.getchildren()) == 1

    msg_node = root.getchildren()[0]
    assert msg_node.tag.lower() == 'message'

    if msg_node.find('PAReq'):
        return decode_pareq(msg_node)
    elif msg_node.find('PARes'):
        return decode_pares(msg_node)

    msg = xml.dom.minidom.parseString(xml_str).toprettyxml(indent='  ')

    raise ValueError(QApplication.translate(None, 'No PAReq or PARes node\n{}'.format(msg)))


def input_to_xml(inp):
    txt = inp.replace('\\r', '').replace('\\n', '').replace('\r', '').replace('\n', '').strip()
    return zlib.decompress(base64.b64decode(txt))


def translate_func(*args, **kwargs):
    return functools.partial(QApplication.translate, *args, **kwargs)


def translate_template(output):
    return re.sub(
        '%%\s*(.+?)\s*%%', lambda m: QApplication.translate(None, m.group(1)), output,
    )


def get_available_translations(langs_dir):
    locales = odict()
    for fname in sorted(os.listdir(langs_dir)):
        fpath = os.path.join(langs_dir, fname)
        if fname.endswith('.qm') and os.path.isfile(fpath):
            name = os.path.splitext(fname)[0]
            locales[name] = os.path.join(langs_dir, fname)

    return locales


class QInputWidget(QTextEdit):
    pure_enter_signal = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setAcceptRichText(False)
        self.setUndoRedoEnabled(True)
        addTranslation(
            self.setPlaceholderText, translate_func(None, 'Insert PAReq or PARes here'),
        )

    def keyPressEvent(self, e):
        if e.isAccepted() and e.key() in (Qt.Key_Enter, Qt.Key_Return):
            if e.modifiers() == Qt.NoModifier:
                self.pure_enter_signal.emit()
                return None
            elif e.modifiers() in (Qt.AltModifier, Qt.ControlModifier, Qt.ShiftModifier):
                # @note: usual Enter key press event without "meta" keys
                s_event = QKeyEvent(QKeyEvent.KeyPress, Qt.Key_Enter, Qt.NoModifier, text='\n')
                # @note: do not handle it twice
                s_event.setAccepted(False)
                # @note: emulate Enter key press event without modifiers
                qt_app.sendEvent(self, s_event)
                return None

        return super().keyPressEvent(e)


class QOutputWidget(QTextEdit):
    template_text = None

    def __init__(self, *args, placeholder=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.setReadOnly(True)
        self.template_text = None
        if placeholder:
            self.setPlaceholderText(placeholder)

    def contextMenuEvent(self, event):
        menu = self.createStandardContextMenu()

        copy_all_act = QAction(QApplication.translate(None, "Copy All"))
        copy_all_act.triggered.connect(self.copyAllToClipboard)
        if self.document().isEmpty():
            copy_all_act.setEnabled(False)
        menu.insertAction(menu.actions()[1], copy_all_act)

        copy_main_act = QAction(QApplication.translate(None, "Copy Main"))
        copy_main_act.triggered.connect(self.copyMainToClipboard)
        if self.document().isEmpty():
            copy_main_act.setEnabled(False)
        menu.insertAction(menu.actions()[2], copy_main_act)

        menu.exec(event.globalPos())

    def copyAllToClipboard(self):
        qt_app.clipboard().setText(self.toPlainText())

    def copyMainToClipboard(self):
        # @note: \n\n - divider between main and certificates infos
        main_info = self.toPlainText().split('\n\n')[0].strip()
        qt_app.clipboard().setText(main_info)

    def setTranslatableTemplate(self, tpl_text):
        self.template_text = tpl_text

    def getTranslatableTemplate(self):
        return self.template_text

    def translateTemplate(self, as_what='html'):
        assert self.template_text is not None

        if as_what == 'html':
            return self.setHtml(translate_template(self.template_text))
        elif as_what == 'plain':
            return self.setPlainText(translate_template(self.template_text))
        else:
            return self.setText(translate_template(self.template_text))


class MainWidget(QWidget):
    def __init__(self, init_input, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initUI(init_input)

    def initUI(self, init_input):
        main_layout = QHBoxLayout()

        self.input_view = QInputWidget()
        self.input_view.pure_enter_signal.connect(self._decode_input)

        self.output_xml_view = QOutputWidget()
        addTranslation(
            self.output_xml_view.setPlaceholderText, translate_func(None, "XML view")
        )
        self.output_info_view = QOutputWidget()
        addTranslation(
            self.output_info_view.setPlaceholderText, translate_func(None, "Main information")
        )

        d_button = QPushButton()
        addTranslation(d_button.setText, translate_func(None, 'Decode'))
        d_button.clicked.connect(self._decode_input)
        dclip_button = QPushButton()
        addTranslation(dclip_button.setText, translate_func(None, 'From clipboard'))
        dclip_button.clicked.connect(self._decode_from_clipboard)
        clear_button = QPushButton()
        addTranslation(clear_button.setText, translate_func(None, 'Clear'))
        clear_button.clicked.connect(self._clear_all)

        grid = QGridLayout()
        grid.setContentsMargins(0, 0, 10, 0)
        grid.setSpacing(10)
        grid.addWidget(self.input_view, 1, 0, 1, 3)
        grid.addWidget(d_button, 2, 0, 1, 1)
        grid.addWidget(dclip_button, 2, 1, 1, 1)
        grid.addWidget(clear_button, 2, 2, 1, 1)
        grid.addWidget(self.output_info_view, 3, 0, 1, 3)

        left_box = QWidget()
        left_box.setLayout(grid)

        h_split = QSplitter(Qt.Horizontal)
        h_split.addWidget(left_box)
        h_split.addWidget(self.output_xml_view)

        main_layout.addWidget(h_split)
        main_layout.setSizeConstraint(QVBoxLayout.SetMinimumSize)

        self.setLayout(main_layout)

        self.input_view.setMinimumWidth(100)
        self.output_xml_view.setMinimumWidth(100)

        self.input_view.grabKeyboard()
        self.input_view.setFocus()
        self.input_view.releaseKeyboard()

        if init_input:
            self.input_view.setPlainText(init_input)
            self._decode_input()

    @pyqtSlot()
    def _decode_from_clipboard(self):
        cboard_text = qt_app.clipboard().text(mode=QClipboard.Clipboard)

        # @note: keep undo/redo history
        curs = QTextCursor(self.input_view.document())
        curs.select(QTextCursor.Document)
        curs.insertText(cboard_text)

        self._decode_input()

    @pyqtSlot()
    def _decode_input(self):
        txt = self.input_view.toPlainText()
        try:
            result_xml = input_to_xml(txt)
            xml_node = xml.dom.minidom.parseString(result_xml)

            f = HtmlFormatter()
            html_out = highlight(xml_node.toprettyxml(indent='  '), XmlLexer(), f)
            doc = QTextDocument('')

            # @note: use current system font (detected by Qt on QApplication __init__)
            doc.setDefaultStyleSheet(pre_style_fmt.format(qt_app.font().family()))
            doc.setHtml(html_out)
            self.output_xml_view.setDocument(doc)

            # @note: show main properties of decoded pareq/pares
            self.output_info_view.setTranslatableTemplate(decode_input(result_xml) or '')
            addTranslation(self.output_info_view.translateTemplate)
        except:
            self.output_xml_view.clear()
            self.output_info_view.clear()

            from traceback import format_exc
            QMessageBox.warning(
                self, QApplication.translate(None, 'Decode error'), format_exc(),
            )

    @pyqtSlot()
    def _clear_all(self):
        self.output_xml_view.clear()
        self.output_info_view.clear()

        curs = QTextCursor(self.input_view.document())
        curs.select(QTextCursor.Document)
        curs.insertText('')

        self.input_view.grabKeyboard()
        self.input_view.setFocus()
        self.input_view.releaseKeyboard()


class MainWindow(QMainWindow):
    tray = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initUI()
        self.initTray()

    def initUI(self):
        addTranslation(self.setWindowTitle, translate_func(None, 'PAReq/PARes view'))
        app_dir = os.path.dirname(os.path.abspath(__file__))
        self.setWindowIcon(QIcon(os.path.join(app_dir, 'icons/3dsinfo-16x16.png')))
        self.setGeometry(0, 0, 1000, 700)

        def get_init_input(qt_app, clipboard_mode):
            cboard_text = qt_app.clipboard().text(mode=clipboard_mode)
            try:
                input_to_xml(cboard_text)
                # @note: no exceptions means that we have what we need
                return cboard_text
            except:
                return None

        # @note: check primary/secondary clipboards
        init_input = get_init_input(qt_app, QClipboard.Clipboard)
        if not init_input:
            init_input = get_init_input(qt_app, QClipboard.Selection)

        main_widget = MainWidget(init_input)
        main_widget.output_xml_view.resize(400, main_widget.output_xml_view.height())
        self.setCentralWidget(main_widget)

        # @note: make menubar
        menu = self.menuBar()
        translations = get_available_translations(langs_dir)
        menu.addMenu(self.createLanguageMenu(translations, parent=menu))
        # @note: set translation to current locale
        current_locale_name = QLocale.system().name()
        self.changeLanguage(translations.get(current_locale_name) or locale_if_no_translation)

        menu.addSeparator()
        about_act = QAction(parent=menu)
        addTranslation(about_act.setText, translate_func(None, 'About'))
        about_act.triggered.connect(self.showAbout)
        menu.addAction(about_act)

        self.show()

    def createLanguageMenu(self, translations, parent=None):
        langs_menu = QMenu(parent=parent)
        addTranslation(langs_menu.setTitle, translate_func(None, 'Language'))

        for locale_name, fpath in translations.items():
            qicon_flag = QIcon('{}.png'.format(os.path.splitext(fpath)[0]))
            act = QAction(qicon_flag, '', self)

            qloc = QLocale(locale_name)
            lang_title = QLocale.languageToString(qloc.language())
            addTranslation(act.setText, translate_func(None, lang_title))
            act.triggered.connect(functools.partial(self.changeLanguage, fpath))
            langs_menu.addAction(act)

        return langs_menu

    @pyqtSlot()
    def changeLanguage(self, fpath):
        if getattr(qt_app, 'current_trans', None):
            current_fpath = qt_app.current_trans.property('fpath')
            # @note: check if switching to current translator
            if current_fpath == fpath:
                return None
            qt_app.removeTranslator(qt_app.current_trans)
            qt_app.current_trans = None

        trans = QTranslator()
        trans.load(fpath)
        trans.setProperty('fpath', fpath)

        qt_app.installTranslator(trans)
        qt_app.current_trans = trans

    def changeEvent(self, event):
        if event.type() == event.LanguageChange:
            for func, trans_func in translators:
                if callable(trans_func):
                    func(trans_func())
                else:
                    func()
            return
        elif event.type() == event.WindowStateChange and self.isMinimized():
            # @warn: little trick - restore to old state then hide as normal
            # @todo: restore window state if it was hidden by window manager
            self.setWindowState(event.oldState())
            self.hide()

        return super().changeEvent(event)

    @pyqtSlot()
    def showAbout(self):
        QMessageBox.about(
            self, QApplication.translate(None, 'About'),
            QApplication.translate(None,
                "<p>&nbsp;&nbsp;&nbsp;The program is intended for viewing PAReq/PARes which "
                "are used in 3-D Secure authentication payment process. Details "
                "<a href=\"https://en.wikipedia.org/wiki/3-D_Secure\">here</a>.</p>"
                "<p>&#8580; <a href=\"https://www.gnu.org/licenses/gpl-2.0.txt\">GPL-2</a> "
                "2019 Kim V.N.</p>",
            )
        )

    def initTray(self):
        self.tray = QSystemTrayIcon()
        self.tray.setIcon(self.windowIcon())

        app_dir = os.path.dirname(os.path.abspath(__file__))

        show_act = QAction(parent=self)
        show_act.setIcon(QIcon(os.path.join(app_dir, 'icons', 'maximize-toggled-prelight.png')))
        addTranslation(show_act.setText, translate_func(None, 'Show/hide window'))
        show_act.triggered.connect(
            lambda: self.show() if not self.isVisible() else self.hide(),
        )

        quit_act = QAction(parent=self)

        quit_act.setIcon(QIcon(os.path.join(app_dir, 'icons', 'quit.png')))
        addTranslation(quit_act.setText, translate_func(None, 'Quit'))
        quit_act.triggered.connect(qt_app.quit)

        m = QMenu()
        m.addAction(show_act)
        m.addAction(quit_act)

        self.tray.setContextMenu(m)
        self.tray.activated.connect(self.trayActivated)
        self.tray.show()

    def trayActivated(self, reason):
        # @todo: handle OSX explicitly
        pass

        if reason in (self.tray.Trigger, self.tray.MiddleClick,):
            if not self.isVisible():
                self.show()
            else:
                self.hide()


if __name__ == '__main__':
    langs_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'langs')

    qt_app = QApplication(sys.argv)
    qt_app.setApplicationName(__appname__)
    qt_app.setApplicationVersion(__version__)

    ex = MainWindow()
    sys.exit(qt_app.exec_())
