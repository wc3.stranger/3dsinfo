# PAReq/PARes view

## Requirements
- Python 3.5+ / pip
- pyOpenSSL
- pygments
- PyQt5

## Install/start (Linux)
```
$ pip install pyOpenSSL Pygments PyQt5
$ python 3dsinfo.py
```
